import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import config from './config';

Vue.use(Router)

export let router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: { requiresAuth: true }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/Login.vue')
    },
    // {
    //   path: '/transfer',
    //   name: 'transfer',
    //   component: () => import('./views/Transfer.vue'),
    //   meta: { requiresAuth: true }
    // },
    {
      path: '/deposit',
      name: 'deposit',
      component: () => import('./views/Deposit.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/withdraw',
      name: 'withdraw',
      component: () => import('./views/Withdraw.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/activities',
      name: 'activities',
      component: () => import('./views/Activities.vue'),
      meta: { requiresAuth: true }
    }
  ],
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  if (!to.matched.some(record => record.meta.requiresAuth)) {
    next()
    return
  }

  if (!config.loggedIn()) {
    next({
      path: '/login',
      query: { redirect: to.fullPath }
    })
    return
  }
  next()
})