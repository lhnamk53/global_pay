import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: colors.lightBlue.darken1,
    secondary: colors.cyan.lighten4,
    accent: colors.grey.darken2,
    error: colors.red.lighten1,
    warning: colors.yellow.darken4,
    info: colors.blue.darken2,
    success: colors.green.lighten1
  }
})
