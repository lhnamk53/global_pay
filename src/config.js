import axios from "axios";

//Private
let apiUrl = 'https://authen.coinyaz.com/api';
let apiFBUrl = 'https://authen.yeaz.win/apiAuthenGlobal/login';

//Public
export default {
  loginFb: async (token) => {
    return await axios(apiFBUrl, {
      method: 'post',
      headers: { 'content-type': 'application/json' },
      data: JSON.stringify({
        api_name: 'login_fb', data: {
          "account_type": 1,
          "fb_token": token
        }
      })
    });
  },
  post: async (apiName, data, header = { 'content-type': 'application/json' }) => {
    return await axios(apiUrl, {
      method: 'post',
      headers: header,
      data: JSON.stringify({ api_name: apiName, data: data })
    });
  },
  loggedIn() {
    const json = localStorage.getItem("rpay:accInfo");
    if (!json) return false;
    try {
      const accInfo = JSON.parse(json);

      if (accInfo.date_expires < new Date().getTime()) {
        localStorage.removeItem("rpay:accInfo");
        return false;
      }
      return true;
    } catch (e) {
      return false;
    }
  },
  logout() {
    localStorage.removeItem("rpay:accInfo");
    location.reload();
  },
  getAccInfo() {
    const json = localStorage.getItem("rpay:accInfo");
    return JSON.parse(json);
  },
  moneyText(val) {
    if (val === 0) return '0';
    if (val == undefined) return '';
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  },
  handleError(code) {
    if (code >= 0 || code == -5) return '';

    switch (code) {
      case -1:
        return "Error";
      case -100:
      case -13:
        return "Syntax error";
      case -99:
      case -101:
        return "System error";
      case -2:
        return "Account is locked";
      case -3:
        return "Phone number is empty";
      case -4:
        return "USDT address is empty";
      case -6:
        return "OTP is incorrect";
      case -7:
        return "Secret key is incorrect";
      case -8:
        return "Sign is incorrect";
      case -9:
        return "Gem is under minimum required";
      case -10:
        return "Gem is not enough";
      case -11:
        return "Token is incorrect";
      case -12:
        return "Session is expired, please login again";
      case -14:
        return "Gem is under minimum required";
      case -15:
        return "Gem is not enough";
      case -16:
        return "Gem after transfer must over 10000";
      default:
        return '';
    }
  }
} 